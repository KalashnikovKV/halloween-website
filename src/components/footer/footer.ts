import "./footer.css";
import impFooterImg from "../assets/images/footerbats.png";
import BeLogo from "../assets/icons/behance.png";
import FigmaLogo from "../assets/icons/Figma.png";
import LiLogo from "../assets/icons/LinkedIn.png";
import InstLogo from "../assets/icons/instagram.png";
import YouTubeLogo from "../assets/icons/youtube.png";
export function AppFooter(): HTMLElement {
  const container: HTMLDivElement = document.createElement("div");
  container.classList.add("FooterContainer");

  // Создание элементов для картинки и текста "halloween"
  const imageContainer = document.createElement("div");
  const image = document.createElement("img");
  image.src = impFooterImg;
  image.classList.add("footerImage");
  const halloweenText = document.createElement("div");
  halloweenText.classList.add("halloweenText");
  halloweenText.dataset.i18 = "footerPhonenumber";

  // Добавление элементов в контейнер картинки
  imageContainer.appendChild(image);
  imageContainer.appendChild(halloweenText);
  container.appendChild(imageContainer);

  // Создание контейнера для иконок-ссылок
  const iconsContainer = document.createElement("div");
  iconsContainer.classList.add("iconsContainer");

  // Создание и добавление иконок-ссылок
  const createIcon = (iconSrc: string) => {
    const icon = document.createElement("img");
    icon.src = iconSrc;
    icon.classList.add("footerIcon");
    iconsContainer.appendChild(icon);
  };

  createIcon(BeLogo);
  createIcon(FigmaLogo);
  createIcon(LiLogo);
  createIcon(InstLogo);
  createIcon(YouTubeLogo);

  container.appendChild(iconsContainer);

  return container;
}
