import "./hero.css";
import impHeroImg from "../assets/images/Hero.png";
import upperTitleHeroCalendarImg from "../assets/icons/calendar.png";
import batImg from "../assets/images/absolute.png";

export function AppHero(): HTMLElement {
  const container: HTMLDivElement = document.createElement("div");
  container.classList.add("heroContainer");

  const upperTitleHero: HTMLDivElement = document.createElement("div");
  upperTitleHero.classList.add("upperTitleHero");

  const upperTitleHeroCalendar: HTMLImageElement =
    document.createElement("img");
  upperTitleHeroCalendar.classList.add("upperTitleHeroCalendarImg");
  upperTitleHeroCalendar.src = upperTitleHeroCalendarImg;
  upperTitleHeroCalendar.alt = "calendar image";

  const upperTitleHeroText: HTMLSpanElement = document.createElement("span");
  upperTitleHeroText.classList.add("upperTitleHeroText");
  upperTitleHeroText.dataset.i18 = "upperTitleHeroText";

  const heroTitle: HTMLSpanElement = document.createElement("span");
  heroTitle.classList.add("heroTitle");
  heroTitle.dataset.i18 = "heroTitle";

  const heroImg: HTMLImageElement = document.createElement("img");
  heroImg.classList.add("heroImg");
  heroImg.src = impHeroImg;
  heroImg.alt = "Halloween house";

  const heroBatImg: HTMLImageElement = document.createElement("img");
  heroBatImg.classList.add("heroBatImg");
  heroBatImg.src = batImg;
  heroBatImg.alt = "Terrifying Bats";

  container.append(upperTitleHero);
  upperTitleHero.append(upperTitleHeroCalendar);
  upperTitleHero.append(upperTitleHeroText);
  container.append(heroTitle);
  container.append(heroImg);
  container.append(heroBatImg);

  return container;
}
