import "./iconwithtext.css";

export function displayTextWithIcons(
  icon1: string,
  icon2: string,
): HTMLElement {
  const container: HTMLDivElement = document.createElement("div");
  container.classList.add("textWithIcons");

  const icon1Element: HTMLImageElement = document.createElement("img");
  icon1Element.src = icon1;
  icon1Element.classList.add("icon");
  container.appendChild(icon1Element);

  const textElement: HTMLSpanElement = document.createElement("div");
  textElement.classList.add("textBetweenIcons");
  textElement.dataset.i18 = "aboutSubtitle";
  container.appendChild(textElement);

  const icon2Element: HTMLImageElement = document.createElement("img");
  icon2Element.src = icon2;
  icon2Element.classList.add("icon");
  container.appendChild(icon2Element);

  return container;
}
