import "./ticket.css";
import ticketIcon from "../assets/icons/ticket.png";
import locationIcon from "../assets/icons/location.png";

export function createTicketSection(): HTMLDivElement {
  const ticketContainer: HTMLDivElement = document.createElement("div");
  ticketContainer.classList.add("ticketContainer");

  const leftDiv: HTMLDivElement = document.createElement("div");
  leftDiv.classList.add("leftDiv");
  leftDiv.style.width = "100px";
  leftDiv.style.height = "90px";
  leftDiv.style.borderRadius = "3px 0 0 3px";

  const ticketImage: HTMLImageElement = document.createElement("img");
  ticketImage.src = ticketIcon;
  ticketImage.classList.add("ticketIcon");
  leftDiv.appendChild(ticketImage);

  const rightDiv: HTMLDivElement = document.createElement("div");
  rightDiv.classList.add("rightDiv");
  rightDiv.style.width = "400px";
  rightDiv.style.height = "90px";
  rightDiv.style.borderRadius = "0 3px 3px 0";

  const textOne: HTMLDivElement = document.createElement("div");
  textOne.dataset.i18 = "ticket1";
  textOne.classList.add("text");
  rightDiv.appendChild(textOne);

  const lineTwo: HTMLDivElement = document.createElement("div");
  lineTwo.classList.add("lineTwo");

  const locationImage: HTMLImageElement = document.createElement("img");
  locationImage.src = locationIcon;
  locationImage.classList.add("locationIcon");
  lineTwo.appendChild(locationImage);
  rightDiv.appendChild(lineTwo);

  const textTwo: HTMLDivElement = document.createElement("div");
  textTwo.dataset.i18 = "ticket2";
  textTwo.classList.add("text");
  lineTwo.appendChild(textTwo);
  rightDiv.appendChild(lineTwo);

  ticketContainer.appendChild(leftDiv);
  ticketContainer.appendChild(rightDiv);

  return ticketContainer;
}
