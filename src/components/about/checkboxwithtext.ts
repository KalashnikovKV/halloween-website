import "./checkboxwithtext.css";

export function displayTextWithCheckbox(
  num: number,
  icon1: string,
): HTMLElement {
  const container: HTMLDivElement = document.createElement("div");
  container.classList.add("textWithIcon");

  const iconElement: HTMLImageElement = document.createElement("img");
  iconElement.src = icon1;
  iconElement.classList.add("icon");
  container.appendChild(iconElement);

  const textElement: HTMLSpanElement = document.createElement("div");
  textElement.classList.add("textWithSingleIcon");
  switch (num) {
    case 1:
      textElement.dataset.i18 = "aboutHunt";
      break;
    case 2:
      textElement.dataset.i18 = "aboutContest";
      break;
    case 3:
      textElement.dataset.i18 = "aboutSweets";
      break;
    case 4:
      textElement.dataset.i18 = "aboutTipsy";
      break;
    case 5:
      textElement.dataset.i18 = "aboutGuest";
      break;
    default:
      textElement.dataset.i18 = "aboutGuest";
      break;
  }
  container.appendChild(textElement);

  return container;
}
