import "./about.css";
import { displayTextWithIcons } from "./iconwithtext";
import { displayTextWithCheckbox } from "./checkboxwithtext";
import { createTicketSection } from "./ticket";

import checkbox from "../assets/icons/tick-square.png";
import arrowr from "../assets/icons/arrow-right.png";
import arrowl from "../assets/icons/arrow-left.png";
import aboutimageweb from "../assets/images/web_from_plans.png";
import aboutskull from "../assets/images/skull.png";

export function AppAbout(): HTMLElement {
  const container: HTMLDivElement = document.createElement("div");
  container.classList.add("AboutContainer");

  const TitleContainer = document.createElement("div");
  TitleContainer.classList.add("TitleContainer");
  const aboutTitle = document.createElement("div");
  aboutTitle.classList.add("TitleText");
  aboutTitle.dataset.i18 = "aboutTitle";
  TitleContainer.appendChild(aboutTitle);

  const aboutSubtitle = displayTextWithIcons(arrowr, arrowl);
  aboutSubtitle.classList.add("Subtitle");
  TitleContainer.appendChild(aboutSubtitle);

  container.appendChild(TitleContainer);

  const AboutInfo = document.createElement("div");
  const AboutInfo1 = document.createElement("div");
  const AboutInfo2 = document.createElement("div");
  const AboutInfo3 = document.createElement("div");
  AboutInfo.classList.add("AboutInfo");
  AboutInfo1.dataset.i18 = "aboutContent1";
  AboutInfo2.dataset.i18 = "aboutContent2";
  AboutInfo3.dataset.i18 = "aboutContent3";
  AboutInfo.appendChild(AboutInfo1);
  AboutInfo.appendChild(AboutInfo2);
  AboutInfo.appendChild(AboutInfo3);

  const Ticket = createTicketSection();
  AboutInfo.appendChild(Ticket);

  const checkboxesfirstline = document.createElement("div");
  const box1 = displayTextWithCheckbox(1, checkbox);
  const box2 = displayTextWithCheckbox(2, checkbox);
  const box3 = displayTextWithCheckbox(3, checkbox);
  checkboxesfirstline.classList.add("checkboxesfirstline");
  checkboxesfirstline.appendChild(box1);
  checkboxesfirstline.appendChild(box2);
  checkboxesfirstline.appendChild(box3);
  AboutInfo.appendChild(checkboxesfirstline);

  const checkboxessecondline = document.createElement("div");
  const box4 = displayTextWithCheckbox(4, checkbox);
  const box5 = displayTextWithCheckbox(5, checkbox);
  checkboxessecondline.classList.add("checkboxesfirstline");
  checkboxessecondline.appendChild(box4);
  checkboxessecondline.appendChild(box5);
  AboutInfo.appendChild(checkboxessecondline);

  container.appendChild(AboutInfo);

  const aboutWebContainer: HTMLDivElement = document.createElement("div");
  aboutWebContainer.classList.add("aboutWeb");
  const aboutWeb: HTMLImageElement = document.createElement("img");
  aboutWeb.src = aboutimageweb;
  aboutWeb.classList.add("AboutSectionWeb");
  aboutWebContainer.appendChild(aboutWeb);
  container.appendChild(aboutWebContainer);

  const aboutSkulContainer: HTMLDivElement = document.createElement("div");
  aboutSkulContainer.classList.add("aboutskul");
  const aboutskullimg: HTMLImageElement = document.createElement("img");
  aboutskullimg.src = aboutskull;
  aboutskullimg.classList.add("AboutSectionSkull");
  aboutSkulContainer.appendChild(aboutskullimg);
  container.appendChild(aboutSkulContainer);

  return container;
}
