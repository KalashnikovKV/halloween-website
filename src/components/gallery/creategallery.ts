import "./creategallery.css";

export function createImageGallery(galleryImages: string[]): HTMLDivElement {
  const galleryContainer: HTMLDivElement = document.createElement("div");
  galleryContainer.classList.add("imageGallery");

  for (let i = 0; i < galleryImages.length; i++) {
    const imageElement: HTMLImageElement = document.createElement("img");
    imageElement.src = galleryImages[i];
    imageElement.classList.add("galleryImage");
    galleryContainer.appendChild(imageElement);
  }

  return galleryContainer;
}
