import "./gallery.css";
import { createImageGallery } from "./creategallery";

import galleryimage1 from "../assets/images/image01.png";
import galleryimage2 from "../assets/images/image02.png";
import galleryimage3 from "../assets/images/image03.png";
import galleryimage4 from "../assets/images/image04.png";
import galleryimage5 from "../assets/images/image05.png";
import galleryimage6 from "../assets/images/image06.png";
import galleryimage7 from "../assets/images/image07.png";
import galleryimage8 from "../assets/images/image08.png";
import galleryimage9 from "../assets/images/image09.png";
import galleryimage10 from "../assets/images/image10.png";
import galleryimagespider from "../assets/images/spider.png";
import galleryimageweb from "../assets/images/web.png";

export function AppGallery(): HTMLElement {
  const container: HTMLDivElement = document.createElement("div");
  container.classList.add("GalleryContainer");

  const halloweenText = document.createElement("div");
  halloweenText.classList.add("halloweenText");
  halloweenText.dataset.i18 = "galleryTitle";
  container.appendChild(halloweenText);

  const imagesArray: string[] = [
    galleryimage1,
    galleryimage2,
    galleryimage3,
    galleryimage4,
    galleryimage5,
    galleryimage6,
    galleryimage7,
    galleryimage8,
    galleryimage9,
    galleryimage10,
  ];

  const galleryContainer: HTMLDivElement = createImageGallery(imagesArray);
  container.appendChild(galleryContainer);

  const gallerySpiderWebContainer: HTMLDivElement =
    document.createElement("div");
  gallerySpiderWebContainer.classList.add("gallerySpiderWeb");

  const gallerySpiderContainer: HTMLDivElement = document.createElement("div");
  gallerySpiderContainer.classList.add("gallerySpider");
  const gallerySpider: HTMLImageElement = document.createElement("img");
  gallerySpider.src = galleryimagespider;
  gallerySpider.classList.add("GallerySpider");

  gallerySpiderContainer.appendChild(gallerySpider);
  gallerySpiderWebContainer.appendChild(gallerySpiderContainer);
  container.appendChild(gallerySpiderWebContainer);

  const galleryWeb: HTMLImageElement = document.createElement("img");
  galleryWeb.src = galleryimageweb;
  galleryWeb.classList.add("GalleryWeb");

  gallerySpiderWebContainer.appendChild(galleryWeb);
  container.appendChild(gallerySpiderWebContainer);

  return container;
}
