import "./reservationBtn.css";

export function reservationBtn(): HTMLButtonElement {
  const reservationBtn: HTMLButtonElement = document.createElement("button");
  reservationBtn.classList.add("reservationBtn");
  reservationBtn.dataset.i18 = "reservationBtn";

  return reservationBtn;
}
