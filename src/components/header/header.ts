import "./header.css";
import { localeBtn } from "./localeBtn/localeBtn";
import { reservationBtn } from "./reservationBtn/reservationBtn";

export function AppHeader(): HTMLDivElement {
  const container: HTMLDivElement = document.createElement("div");
  container.classList.add("header-container");

  const headerLogo: HTMLSpanElement = document.createElement("span");
  headerLogo.classList.add("headerLogo");
  headerLogo.dataset.i18 = "header-title";

  const navMenu: HTMLDivElement = document.createElement("div");
  navMenu.classList.add("navMenu");

  const navMenu_1: HTMLSpanElement = document.createElement("span");
  navMenu_1.dataset.i18 = "navMenu_1";

  const navMenu_2: HTMLSpanElement = document.createElement("span");
  navMenu_2.dataset.i18 = "navMenu_2";

  const navMenu_3: HTMLSpanElement = document.createElement("span");
  navMenu_3.dataset.i18 = "navMenu_3";

  const navMenu_4: HTMLSpanElement = document.createElement("span");
  navMenu_4.dataset.i18 = "navMenu_4";

  const navMenu_5: HTMLSpanElement = document.createElement("span");
  navMenu_5.dataset.i18 = "navMenu_5";

  container.append(headerLogo);
  container.append(navMenu);
  navMenu.append(navMenu_1);
  navMenu.append(navMenu_2);
  navMenu.append(navMenu_3);
  navMenu.append(navMenu_4);
  navMenu.append(navMenu_5);
  container.append(localeBtn());
  container.append(reservationBtn());

  return container;
}
