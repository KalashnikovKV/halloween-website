import "./localeBtn.css";
import { changeCurrLocale } from "../../../locale-service";

export function localeBtn(): HTMLButtonElement {
  const button: HTMLButtonElement = document.createElement("button");

  button.innerText = "EN عربي";
  button.classList.add("localeBtn");
  button.addEventListener("click", () => {
    changeCurrLocale();
  });

  return button;
}
