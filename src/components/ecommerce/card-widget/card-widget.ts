import "./card-widget.css";

export interface CardWidgetProps {
  num: "even" | "odd";
  Title?: string;
  i18Title: string;
  card_Price: string;
  drink?: string;
  i18Drink: string;
  activity_1?: string;
  i18Act1: string;
  activity_2?: string;
  i18Act2: string;
}

export function cardWidget(props: CardWidgetProps): HTMLDivElement {
  const container: HTMLDivElement = document.createElement("div");

  if (props.num === "even") {
    container.classList.add("cardWidgetContainerEven");
  } else {
    container.classList.add("cardWidgetContainerOdd");
  }

  const cardTypeTicket: HTMLSpanElement = document.createElement("span");
  cardTypeTicket.innerText = props.Title!;
  cardTypeTicket.classList.add("cardTitle");
  cardTypeTicket.dataset.i18 = props.i18Title;

  const cardPrice: HTMLSpanElement = document.createElement("span");
  cardPrice.innerText = props.card_Price;
  cardPrice.classList.add("cardPrice");

  const cardDrink: HTMLSpanElement = document.createElement("span");
  cardDrink.innerText = props.drink!;
  cardDrink.classList.add("cardSubtitles");
  cardDrink.dataset.i18 = props.i18Drink;

  const cardAct1: HTMLSpanElement = document.createElement("span");
  cardAct1.innerText = props.activity_1!;
  cardAct1.classList.add("cardSubtitles");
  cardAct1.dataset.i18 = props.i18Act1;

  const cardAct2: HTMLSpanElement = document.createElement("span");
  cardAct2.innerText = props.activity_2!;
  cardAct2.classList.add("cardSubtitles");
  cardAct2.dataset.i18 = props.i18Act2;

  const cardBtn: HTMLButtonElement = document.createElement("button");
  cardBtn.classList.add("cardBtn");

  if (props.num === "even") {
    cardBtn.classList.add("cardBtnEven");
  } else {
    cardBtn.classList.add("cardBtnOdd");
  }

  cardBtn.dataset.i18 = "cardBtn";

  container.append(cardTypeTicket);
  container.append(cardPrice);
  container.append(cardDrink);
  container.append(cardAct1);
  container.append(cardAct2);
  container.append(cardBtn);

  return container;
}
