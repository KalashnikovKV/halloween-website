import "./ecommerce.css";
import { cardWidget, CardWidgetProps } from "./card-widget/card-widget";

export function AppEcommerce(): HTMLDivElement {
  const container: HTMLDivElement = document.createElement("div");
  container.classList.add("AppEcommerceContainer");

  const EcommTitle: HTMLSpanElement = document.createElement("span");
  EcommTitle.classList.add("EcommTitle");
  EcommTitle.dataset.i18 = "EcommTitle";

  const cardsContainer: HTMLDivElement = document.createElement("div");
  cardsContainer.classList.add("cardsContainer");

  container.append(EcommTitle);
  container.append(cardsContainer);

  const addCard = (props: CardWidgetProps): void => {
    const card = cardWidget(props);
    cardsContainer.append(card);
  };

  addCard({
    i18Title: "leftCardTitle",
    card_Price: "$100",
    i18Drink: "cardDrink",
    i18Act1: "right_Act_1",
    i18Act2: "right_Act_2",
    num: "odd",
  });

  addCard({
    i18Title: "middleCardTitle",
    card_Price: "$150",
    i18Drink: "cardDrink",
    i18Act1: "middle_Act_1",
    i18Act2: "middle_Act_2",
    num: "even",
  });

  addCard({
    i18Title: "rightCardTitle",
    card_Price: "$300",
    i18Drink: "cardDrink",
    i18Act1: "left_Act_1",
    i18Act2: "left_Act_2",
    num: "odd",
  });

  return container;
}
