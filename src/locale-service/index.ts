import enLocale from "../resources/en.json";
import arLocale from "../resources/ar.json";

interface Locale {
  [key: string]: string;
}

export function getCurrLocale(locale: string): Promise<Locale> {
  return new Promise((resolve) => {
    setTimeout(() => {
      if (locale === "en") {
        return resolve(enLocale);
      } else {
        return resolve(arLocale);
      }
    }, 500);
  });
}

export const changeCurrLocale = async () => {
  const currLocale = localStorage.getItem("locale");

  if (currLocale === "en" || !currLocale) {
    const locales = await getCurrLocale("ar");
    document.querySelector("html")!.dir = "rtl";
    localStorage.setItem("locale", "ar");

    changeLocale(locales);
  } else {
    const locales = await getCurrLocale("en");
    document.querySelector("html")!.dir = "ltr";
    localStorage.setItem("locale", "en");
    changeLocale(locales);
  }
};

export const changeLocale = (locales: Locale) => {
  const elsForTranslate = document.querySelectorAll<HTMLElement>("[data-i18]");

  elsForTranslate.forEach((el) => {
    const key = el.getAttribute("data-i18");
    if (key) {
      el.innerText = locales[key];
    }
  });
};

export const initTranslate = async () => {
  const localees = await getCurrLocale("en");
  changeLocale(localees);
};
