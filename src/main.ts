import "./styles.css";
import { AppHeader } from "../src/components/header/header";
import { initTranslate } from "./locale-service";
import { AppHero } from "./components/hero/hero";
import { AppEcommerce } from "./components/ecommerce/ecommerce";
import { AppGallery } from "./components/gallery/gallery";
import { AppAbout } from "./components/about/about";
import { AppFooter } from "./components/footer/footer";

const app: HTMLElement | null = document.querySelector("#app");

if (app) {
  app.append(AppHeader());
  app.append(AppHero());
  app.append(AppGallery());
  app.append(AppAbout());
  app.append(AppEcommerce());
  app.append(AppFooter());

  initTranslate();
}
